$(document).ready(() => {

  /**
   * Swiper sliders
   */

   var swiper = new Swiper('.s-featured-tours__slider', {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    navigation: {
      prevEl: '.s-featured-tours__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-featured-tours__slider-nav .s-slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        pagination: {
          el: '.s-featured-tours__slider-progressbar',
          type: 'progressbar'
        }
      },
      768: {
        pagination: {
          el: '.s-featured-tours__slider-fraction',
          type: 'fraction'
        }
      }
    }
  });

   var swiper = new Swiper('.s-featured-news__slider', {
    pagination: {
      el: '.s-featured-news__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.s-featured-news__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-featured-news__slider-nav .s-slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      1200: {
        slidesPerView: 2,
        spaceBetween: 30
      }
    }
  });

  var swiper = new Swiper('.s-featured-partners__slider', {
    pagination: {
      el: '.s-featured-partners__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.s-featured-partners__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-featured-partners__slider-nav .s-slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 30
      },
    }
  });

  var swiper = new Swiper('.s-gallery__slider', {
    pagination: {
      el: '.s-gallery__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.s-gallery__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-gallery__slider-nav .s-slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  });
});